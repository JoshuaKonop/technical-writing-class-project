# Technical Writing Class Project

This is my completed final project for ENGLISH 402, technical writing.

In this project, we (Group of 5) were tasked with collecting data on some topic of our choosing and creating a visualizion for that topic. We decided to observe people's descriptions about their day and how it relates to their stress level and amount of work/homework/club/other activities. I created a google forms that collected the data, downloaded a csv file, and used python to parse/clean the data. Then I used python to generate a wordcloud for each day to visualize how often a word was used.
