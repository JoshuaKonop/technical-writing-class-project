# Python program to generate WordCloud

# importing all necessary modules
from time import sleep
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import pandas as pd

#Additional words to filter out
filterWords = ("im", "ive", "day", "going", "kinda", "four", "next", "show", "catch", "got", "pretty", "now", "today", "two")
#goodWords = {"good": 2, "great": 4, "solid": 1.5, "alright": 1}
#badWords = {"unproductive": -1, "hard": -1, "tiring": -1, "exam": -2, "bad": -2, "exhausting": -1, "homework": -1, "tired": -2, "sucked": -1, "miserable": -4, "stressed": -2, "stressful": -2}
#multipliers = {"feeling": 1.25, "super": 2, "never": -1, "really": 2, "not": -1, "very": 2, "lot": 1.5, "kinda": 0.75}
#filterWords = ()

#Read from initial dataset
infile = open("text.csv", "r")
z = []
x = infile.readline()	#Get first 2 lines
x = infile.readline()

while infile:
	x = infile.readline()
	y = x.split(",", 1)
	#print("Y IS CURRENTLY")
	#print(y)
	if y[0] != '':
		print(y[1])
		if y[1].startswith("\""):
			a = y[1].split("\"")
			#print(a)
			b = a[1]
			b = b.replace(",", " the ")
			z.append(b)
		else:
			a = y[1].split(",",1)
			#print(a[0])
			b = a[0]
			z.append(b)
	else:
		break	

#print("IN Z")
#print(z)
infile.close()
outfile = open("out.csv", "w")
counter = 0
for a in z:
	b = a.split(" ")
	#print(a)
	for i in b:
		i = str(i)
		if i.startswith("\""):
			i = i[1:len(i)]
		if i.endswith("\""):
			i =i[0:len(i)-1]
		#print(i)
		if "." in i:
			if i.split(".")[1] != '':
				i = i.replace(".", " ")
			i = i.replace(".","")
		
		if "," in i:
			if i.split(",")[1] != '':
				i = i.replace(",", " ")
			i = i.replace(",","")

		i = i.replace(":","")
#		i = i.replace(",","")
		i = i.replace("?","")
		i = i.replace("!","")
		i = i.replace("'","")
		
		print(i)
		
		if i == "":	#If parsing out special characters makes i empty, replace it with an extra the.
			i = "the"
		
		#print(i.lower())
		if i.lower() in filterWords:
			if i.lower() == "day":
				print("HERE")
				print(i)
			i = "the"

		outfile.write(i)
		if counter == 0:
			outfile.write(",")
			counter+=1
		elif counter == 1:
			outfile.write("\n")
			counter-=1
	#print(b)

if counter == 1:
	pass
	outfile.write("the\n")	

#Pandas requires all csv entries to have the same number of columns. In order to do this, I create a new csv with the text inputs, with
#each row being 2 columns (this will create a ton of columns, but assures even columns). If there is an odd number of inputs, this if statement
#will catch that and pad the final column with an extra the. The the will be parsed out when the wordcould is calculated
outfile.write("")	
outfile.close()
#sleep(1)
df = pd.read_csv(r"out.csv", encoding ="latin-1")

comment_words = ''
stopwords = set(STOPWORDS)

# iterate through the csv file
for val in df.values:
	
	# typecaste each val to string
	val = str(val)
	
	# split the value
	tokens = val.split()
	
	# remove additional characters
	for x in range(0, len(tokens)):
		tokens[x] = tokens[x].replace("'","")

	#print(tokens)
	# Converts each token into lowercase
	for i in range(len(tokens)):
		tokens[i] = tokens[i].lower()
	
	comment_words += " ".join(tokens)+" "

wordcloud = WordCloud(width = 800, height = 800,
				background_color ='black',
				colormap='Reds',
				stopwords = stopwords,
				min_font_size = 10).generate(comment_words)

# plot the WordCloud image					
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)

plt.show()

wordcloud = WordCloud(width = 800, height = 800,
				background_color ='black',
				colormap='ocean',
				stopwords = stopwords,
				min_font_size = 10).generate(comment_words)

# plot the WordCloud image					
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)

plt.show()

wordcloud = WordCloud(width = 800, height = 800,
				background_color ='black',
				colormap='summer',
				stopwords = stopwords,
				min_font_size = 10).generate(comment_words)

# plot the WordCloud image					
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)

plt.show()