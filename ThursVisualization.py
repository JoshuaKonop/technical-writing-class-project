from time import sleep
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import pandas as pd


#Read from initial dataset
infile = open("text.csv", "r")
outfile = open("parsed.csv", "w")
x = infile.readline()
x = infile.readline()
li = []
filterWords = ("and", "am", "was", "of", "has", "so", "a", "i", "to", "it", "my", "im", "ive", "day", "going", "kinda", "four", "next", "show", "catch", "got", "pretty", "now", "today", "two")

while infile:
	x = infile.readline()
	if x == "":
		break
	li.append(x)

dates = {"": []}
for x in li:
	y = x.split(",", 1)
#	print(y)
	a = str(y[0]).split(" ")[0]
#	print(a)
	if a in dates.keys():
		dates[a].append(y[1])
	else:
		dates[a] = [y[1]]

dKeys = dates.keys()

finalDict = {}
lliDict = {}
for x in dKeys:
	lli = []
	lli2 = []
	finalDict[x] = {}
	for y in dates[x]:
		if y[0] == '\"':
			a = y.split("\"")
			#print(a[2])
			lli.append(a[1])
			lli2.append(a[2])
		else:
			a = y.split(",", 1)
			#print(a[1])
			lli.append(a[0])
			lli2.append(a[1])

	lliDict[x] = lli2

	for a in lli:
		for i in a.split(" "):
			i = str(i).lower()
			if i.startswith("\""):
				i = i[1:len(i)]
			if i.endswith("\""):
				i =i[0:len(i)-1]
			#print(i)
			if "." in i:
				if i.split(".")[1] != '':
					i = i.replace(".", " ")
				i = i.replace(".","")
			
			if "," in i:
				if i.split(",")[1] != '':
					i = i.replace(",", " ")
				i = i.replace(",","")

			i = i.replace(":","")
		#		i = i.replace(",","")
			i = i.replace("?","")
			i = i.replace("!","")
			i = i.replace("'","")
			i = i.replace("’","")
			
			#print(i)
			
			if i == "":	#If parsing out special characters makes i empty, replace it with an extra the.
				i = "the"
			
			#print(i.lower())
			if i.lower() in filterWords:
				i = "the"

			if i in finalDict[x].keys():
				finalDict[x][i] +=1
			else:
				finalDict[x][i] = 1

		finalDict[x] = {key: val for key, val in sorted(finalDict[x].items(), key = lambda ele: ele[1], reverse=True)}
#print(finalDict)


#print(lliDict)
for x in lliDict.keys():
	entries = 0
	statsDict = {"meanTotalTime": 0, "meanClassTime": 0, "meanWorkTime": 0, "meanHomeworkTime": 0, "meanClubTime": 0, "meanStress": 0, "meanAssignments": 0}
	for a in lliDict[x]:
		a = str(a)
		a= a.split(",")

		z = []
		for b in a:
			b = str(b)
			b = b.replace("\n", "")
			if b != "":
				z.append(b)
		a = z
		#print(a)
		statsDict["meanTotalTime"] += (float(a[0]) + float(a[1]) + float(a[2]) + float(a[3]))
		statsDict["meanClassTime"] += float(a[0])
		statsDict["meanWorkTime"] += float(a[1])
		statsDict["meanClubTime"] += float(a[2])
		statsDict["meanHomeworkTime"] += float(a[3])
		statsDict["meanAssignments"] += float(a[4])
		statsDict["meanStress"] += float(a[5])
		entries+=1
	
	if statsDict["meanTotalTime"] != 0:
		statsDict["meanTotalTime"] = (statsDict["meanTotalTime"]/entries)
	if statsDict["meanClassTime"] != 0:
		statsDict["meanClassTime"] = (statsDict["meanClassTime"]/entries)
	if statsDict["meanWorkTime"] != 0:
		statsDict["meanWorkTime"] = (statsDict["meanWorkTime"]/entries)
	if statsDict["meanClubTime"] != 0:
		statsDict["meanClubTime"] = (statsDict["meanClubTime"]/entries)
	if statsDict["meanHomeworkTime"] != 0:
		statsDict["meanHomeworkTime"] = (statsDict["meanHomeworkTime"]/entries)
	if statsDict["meanAssignments"] != 0:
		statsDict["meanAssignments"] = (statsDict["meanAssignments"]/entries)
	if statsDict["meanStress"] != 0:
		statsDict["meanStress"] = (statsDict["meanStress"]/entries)
	
	#print(statsDict)
	lliDict[x] = statsDict


highstress = {"": 0}
moderatestress = {"": 0}
lowstress = {"": 0}
for x in finalDict.keys():
	if lliDict[x]["meanStress"] > 3:
		for y in finalDict[x].keys():
			#print(y)
			if y in highstress.keys():
				highstress[y]+=1
			else:
				highstress[y] = 1
	elif lliDict[x]["meanStress"] == 3:
		for y in finalDict[x].keys():
			#pass
			if y in moderatestress.keys():
				moderatestress[y]+=1
			else:
				moderatestress[y] = 1
	elif lliDict[x]["meanStress"] < 3:
		for y in finalDict[x].keys():
			#pass
			if y in lowstress.keys():
				lowstress[y]+=1
			else:
				lowstress[y] = 1
highstress = dict(sorted(highstress.items(), key=lambda x:x[1], reverse=True))
moderatestress = dict(sorted(moderatestress.items(), key=lambda x:x[1], reverse=True))
lowstress = dict(sorted(lowstress.items(), key=lambda x:x[1], reverse=True))

print(highstress)
print("\n\n\n")
print(moderatestress)
print("\n\n\n")
print(lowstress)
print("\n\n\n")
wordfile = open("wordfile.csv", "w")

wordfile.write("HIGH STRESS\n")
for x in highstress.keys():
	wordfile.write(x)
	wordfile.write(",")
	wordfile.write(str(highstress[x]))
	wordfile.write(",\n")

wordfile.write("\nMODERATE STRESS\n")
for x in moderatestress.keys():
	wordfile.write(x)
	wordfile.write(",")
	wordfile.write(str(moderatestress[x]))
	wordfile.write(",\n")

wordfile.write("\nLOW STRESS\n")
for x in lowstress.keys():
	wordfile.write(x)
	wordfile.write(",")
	wordfile.write(str(lowstress[x]))
	wordfile.write(",\n")

wordfile.close()




for x in finalDict.keys():
	outfile.write(x)
	outfile.write(", ")
	
	for a in finalDict[x].keys():
		outfile.write(a)
		outfile.write(", ")
		outfile.write(str(finalDict[x][a]))
		outfile.write(", ")
		#pass
	for a in lliDict[x].keys():
		outfile.write(a)
		outfile.write(", ")
		outfile.write(str(lliDict[x][a]))
		outfile.write(", ")
	
	outfile.write("\n")

skipfirst = 0
for x in finalDict.keys():
	print(x)
	print(lliDict[x])
	if skipfirst == 0:
		skipfirst+=1
		continue
	outfile2 = open("out.csv", "w")
	listForDay = []
	counter = 0
	for a in finalDict[x].keys():
		#print(finalDict[x][a])
		for z in range(0, finalDict[x][a]):
			#print(z)
			if counter == 0:
				outfile2.write(a)
				outfile2.write(",")
				counter+=1
			elif counter == 1:
				outfile2.write(a)
				outfile2.write("\n")
				counter-=1
	if counter == 1:
		outfile2.write("the\n")
	outfile2.close()

	df = pd.read_csv(r"out.csv", encoding ="latin-1")

	comment_words = ''
	stopwords = set(STOPWORDS)

	# iterate through the csv file
	for val in df.values:
		
		# typecaste each val to string
		val = str(val)
		
		# split the value
		tokens = val.split()
		
		# remove additional characters
		for g in range(0, len(tokens)):
			tokens[g] = tokens[g].replace("'","")

		#print(tokens)
		# Converts each token into lowercase
		for i in range(len(tokens)):
			tokens[i] = tokens[i].lower()
		
		comment_words += " ".join(tokens)+" "

	wordcloud = WordCloud(width = 800, height = 800,
					background_color ='black',
					colormap='Greys',
					stopwords = stopwords,
					min_font_size = 10).generate(comment_words)

	if (lliDict[x]["meanStress"] > 3):
		wordcloud = WordCloud(width = 800, height = 800,
						background_color ='black',
						colormap='autumn',
						stopwords = stopwords,
						min_font_size = 10).generate(comment_words)
	elif (lliDict[x]["meanStress"] < 3):
		wordcloud = WordCloud(width = 800, height = 800,
						background_color ='black',
						colormap='summer',
						stopwords = stopwords,
						min_font_size = 10).generate(comment_words)
	elif (lliDict[x]["meanStress"] == 3):
		wordcloud = WordCloud(width = 800, height = 800,
						background_color ='black',
						colormap='cool',
						stopwords = stopwords,
						min_font_size = 10).generate(comment_words)

	# plot the WordCloud image					
	plt.figure(figsize = (8, 8), facecolor = None)
	plt.imshow(wordcloud)
	plt.axis("off")
	plt.tight_layout(pad = 0)

	plt.show()
	

	# print(dates)
	# z.append(y[0])
	# if y[0] != '':
	# 	if y[1].startswith("\""):
	# 		a = y[1].split("\"")
	# 		print(a)
	# 		z.append("Text Here")
	# 		z.append(a[2])
	# 	else:
	# 		a = y[1].split(",", 1)
	# 		z.append("Text Here")
	# 		z.append(a[1])
	# else:
	# 	break	

	# print(z)
	# counter = 0
	# for x in z:
	# 	if counter == 2:
	# 		outfile.write(x)
	# 		counter = 0
	# 	else:
	# 		outfile.write(x + ",")
	# 		counter+=1



def a(a):
	if a != "":
		return True
	else:
		return False